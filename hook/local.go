// this is used for locally triggered builds by using the cli for mvoCI.

package hook

type LocalPayload struct {
    Action string
    Secret string
    RepositoryID uint
    RepositoryName string
}
